﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class SendTaskEventHandler : IEventHandler<SendTaskEventData>
    {
        public void Handle(SendTaskEventData data)
        {
            Console.WriteLine("SendTask");
        }
    }
}
