﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class TaskFinishedEventHandler : IEventHandler<TaskFinishedEventData>
    {
        public void Handle(TaskFinishedEventData data)
        {
            Console.WriteLine("TaskFinished");
        }
    }
}
