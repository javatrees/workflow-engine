﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using WorkFlowCore.Conditions;

namespace WorkFlowCore.Framework.Conditions
{
    [Condition("一般条件处理器", "!=|>=|<=|<|>|==|⊆|∈，参数被 value() 方法包含时解析")]
    public class GeneralCondition : ICondition
    {
        private VType GetValue<VType>(string expression,string formData)
        {
            //被 value() 方法包含时，需要解析，否则原值返回
            var regex = new Regex(@"value\((?<value>.*)\)");

            if (regex.IsMatch(expression))

            {
                JObject jObject = JObject.Parse(formData);
                var token = jObject.SelectToken(regex.Match(expression).Groups["value"].Value);
                return token.Value<VType>();
            }
            else return (VType)Convert.ChangeType(expression,typeof(VType));
        }
        public bool CanAccept(ConditionInput input)
        {
            try
            {
                //简单的表达式解析

                var queryExpressions = input.Expression.Split(new string[] { "&&" }, StringSplitOptions.None);
                var regexy = new Regex("(?<operation>!=|>=|<=|<|>|==|⊆|∈)");

                var paramList = new List<object>();
                
                foreach (var expression in queryExpressions)
                {
                    if (!regexy.IsMatch(expression)) continue;
                    var operation = regexy.Match(expression).Groups["operation"].Value;

                    var expressionInfo = expression.Split(new string[] { operation }, StringSplitOptions.None);
                    if (expressionInfo.Length != 2) continue;

                    var key = expressionInfo[0].Trim();
                    var keyValue = GetValue<string>(key, input.WorkTask.FormData) ;//TODO 函数解析key获取数据
                    var value = expressionInfo[1].Trim();
                    var valueValue = GetValue<string>(value, input.WorkTask.FormData);//TODO 函数解析value获取数据
                    var result = false;

                    switch (operation)
                    {
                        case "!=":
                            result = !keyValue.Equals(valueValue);
                            break;
                        case ">=":
                            result = decimal.Parse(keyValue)>=decimal.Parse(valueValue);
                            break;
                        case "<=":
                            result = decimal.Parse(keyValue) <= decimal.Parse(valueValue);
                            break;
                        case ">":
                            result = decimal.Parse(keyValue) > decimal.Parse(valueValue);
                            break;
                        case "<":
                            result = decimal.Parse(keyValue) < decimal.Parse(valueValue);
                            break;
                        case "==":
                            result = keyValue.Equals(valueValue);
                            break;
                        case "⊆"://包含
                            result = keyValue.Contains(valueValue);
                            break;
                        case "∈"://属于
                            result = valueValue.Contains(keyValue);
                            break;
                        default:
                            break;
                    }
                    if (!result) return false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
