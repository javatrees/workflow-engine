﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Conditions;

namespace WorkFlowCore.Framework.Conditions
{
    [Condition("条件处理器A")]
    public class ConditionA : ICondition
    {
        public bool CanAccept(ConditionInput input)
        {
            try
            {
                //简单的表达式解析
                var keyvalue = input.Expression.Split('=');
                JObject jObject = JObject.Parse(input.WorkTask.FormData);
                var token = jObject.SelectToken(keyvalue[0]);
                var value = token.Value<string>();
                return value.Equals(keyvalue[1]);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
