﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Authorization;
using WorkFlowCore.Framework.UserSelectors;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Framework.Authorization
{
    public class DefaultSession : IWorkflowSession
    {
        public DefaultSession(IHttpContextAccessor httpContextAccessor)
        {
            //TODO通过请求上下文获取用户凭据
            //var token =httpContextAccessor.HttpContext.Request.Headers["token"];

            //此处不做具体实现。模拟返回其中一个用户
            var user = UserList.Users[0];
            this.User = new User(user.Id, user.Name);
        }

        public User User { get; private set; }
    }
}
