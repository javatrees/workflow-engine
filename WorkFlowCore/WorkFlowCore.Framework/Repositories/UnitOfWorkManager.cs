﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;
using Microsoft.Extensions.DependencyInjection;

namespace WorkFlowCore.Framework.Repositories
{
    public class UnitOfWorkManager : IUnitOfWorkManager
    {
        private readonly UnitOfWork unitOfWork;

        public UnitOfWorkManager(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = (UnitOfWork)unitOfWork;
        }

        public IUnitOfWork Begin()
        {
            unitOfWork.Begin();
            return unitOfWork;
        }
    }
}
