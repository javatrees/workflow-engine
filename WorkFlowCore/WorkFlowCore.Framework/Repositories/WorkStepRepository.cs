﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.IRepositories;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.Repositories
{
    public class WorkStepRepository : BasicRepository<WorkStepInfo, Guid>, IWorkStepRepository
    {
        public WorkStepRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<PageResult<WorkStep>> GetUnHandlerWorkStepsOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            var result = new PageResult<WorkStep>
            {
                Total = (await GetCountAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled))
            };
            if (pageSize < 1)
                result.Items = (await GetListAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled)).Select(w => w.ToWorkStep()).ToList();
            else result.Items = (await GetPagedListAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled,(pageIndex-1)*pageSize,pageSize,"")).Select(w => w.ToWorkStep()).ToList();
            return await Task.FromResult(result);
        }
    }
}
