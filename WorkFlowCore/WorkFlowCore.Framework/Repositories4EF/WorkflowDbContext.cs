﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class WorkflowDbContext : DbContext
    {
        public WorkflowDbContext([NotNull] DbContextOptions<WorkflowDbContext> options) : base(options)
        {
        }

        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowVersionInfo> WorkflowVersionInfos { get; set; }
        public DbSet<WorkStepInfo> WorkStepInfos { get; set; }
        public DbSet<WorkTaskInfo> WorkTaskInfos { get; set; }
    }
}
