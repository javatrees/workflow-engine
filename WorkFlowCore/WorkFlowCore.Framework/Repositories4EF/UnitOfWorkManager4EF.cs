﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;
using Microsoft.Extensions.DependencyInjection;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class UnitOfWorkManager4EF : IUnitOfWorkManager
    {
        private readonly UnitOfWork4EF unitOfWork;

        public UnitOfWorkManager4EF(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = (UnitOfWork4EF)unitOfWork;
        }

        public IUnitOfWork Begin()
        {
            unitOfWork.Begin();
            return unitOfWork;
        }
    }
}
