﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Framework;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Test.Repositories
{
    public class Repositories_Test
    {
        private IServiceProvider serviceProvider;
        [SetUp]
        public void SetUp()
        {
            ServiceCollection services = new ServiceCollection();
            services.AddWorkFlowCoreFramework();
            services.AddTransient<TestEntity>();
            serviceProvider = services.BuildServiceProvider();
        }

        [Test]
        public void CURD()
        {
            var testRepository = serviceProvider.GetService<IBasicRepository<TestEntity>>();
            var result =testRepository.InsertAsync(new TestEntity() { Number=111,Name="name111"}).Result;
            Assert.IsTrue(testRepository.GetListAsync().Result.Where(t => t.Number == 111).Any());
        }

        [Test]
        public void CURDWithUnitOfWorkCommit()
        {
            var unitOfWorkManager =serviceProvider.GetService<IUnitOfWorkManager>();
            using(var unitOfWork = unitOfWorkManager.Begin())
            {
                var testRepository = serviceProvider.GetService<IBasicRepository<TestEntity>>();
                var result = testRepository.InsertAsync(new TestEntity() { Number = 222, Name = "name222" }).Result;
                unitOfWork.Commit();
                Assert.IsTrue(testRepository.GetListAsync().Result.Where(t => t.Number == 222).Any());
            }
        }

        [Test]
        public void CURDWithUnitOfWorkNotCommit()
        {
            var unitOfWorkManager = serviceProvider.GetService<IUnitOfWorkManager>();
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                var testRepository = serviceProvider.GetService<IBasicRepository<TestEntity>>();
                var result = testRepository.InsertAsync(new TestEntity() { Number = 333, Name = "name333" }).Result;
                Assert.IsTrue(!testRepository.GetListAsync().Result.Where(t => t.Number == 333).Any());
            }
        }



        public class TestEntity: WithBaseInfoEntity
        {
            public int Number { get; set; }
            public string Name { get; set; }

            public object[] GetKeys()
            {
                return GetType().GetProperties().Select(p => p.Name).ToArray();
            }
        }
    }
}
