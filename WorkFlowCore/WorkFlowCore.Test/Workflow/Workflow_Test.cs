﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkFlowCore.Conditions;
using WorkFlowCore.Framework;
using WorkFlowCore.Framework.Repositories;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Test.Workflow.Conditions;
using WorkFlowCore.Test.Workflow.UserSelectors;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Test.Workflow
{
    public class Workflow_Test
    {
        private IServiceProvider serviceProvider;
        [SetUp]
        public void Setup()
        {
            ServiceCollection services = new ServiceCollection();
            UserSelectorManager.RegisterSelector(GetType().Assembly);
            ConditionManager.Registercondition(GetType().Assembly);

            services.AddWorkFlowCore(config =>
            {
                config.RegisterSelector(GetType().Assembly);
                config.Registercondition(GetType().Assembly);
            });

            services.AddWorkFlowCoreFramework();
            services.AddScoped<ConditionA>();
            services.AddScoped<UserSelectorA>();

            BasicRepository<Workflows.Workflow, Guid>.ClearData();
            BasicRepository<WorkflowVersion, Guid>.ClearData();
            BasicRepository<WorkTask, Guid>.ClearData();
            BasicRepository<WorkStep, Guid>.ClearData();

            serviceProvider = services.BuildServiceProvider();
        }

        [Test]
        public void CreateWorkflow()
        {
            var workflowManager = serviceProvider.GetService<WorkflowManager>();

            workflowManager.CreateWorkflow("wfno", "name", "des").Wait();

            var workflowRepository = serviceProvider.GetService<IBasicRepository<Workflows.Workflow>>();
            var wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;
            Assert.IsTrue(wf.WorkflowNo.Equals("wfno"));

        }
        [Test]
        public void UpdateWorkflow()
        {
            var workflowManager = serviceProvider.GetService<WorkflowManager>();

            CreateWorkflow();

            var workflowRepository = serviceProvider.GetService<IBasicRepository<Workflows.Workflow>>();

            var wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;


            WorkflowId workflowId = new WorkflowId(wf.ActiveVersion, wf.Id);
            var workflowNodes = new List<WorkflowNode>();

            var userSelector = new NodeUser(typeof(UserSelectorA).FullName, "UserSelectorA", new List<UserSelection> { new UserSelection { Id = "creator", Name = "创建人" } }, "选择参数", "des", NodeUser.NodeHandleType.Handle);


            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "开始节点", WorkNodeType.Begin, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "分叉节点", WorkNodeType.HandOut, string.Empty, false,
               new List<NodeUser> {
               userSelector
           }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "普通节点", WorkNodeType.Normal, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "普通节点1", WorkNodeType.Normal, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "会签节点", WorkNodeType.Sign, string.Empty, false,
              new List<NodeUser> {
               userSelector
            }, null));


            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "结束节点", WorkNodeType.End, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            var workflowLines = new List<WorkflowLine>();

            workflowLines.Add(new WorkflowLine("开始-分叉", workflowId, workflowNodes[0].Id, workflowNodes[1].Id, new List<LineCondition>
            {
                //new LineCondition("ConditionA","ConditionA","x=1","testdes")
            }));

            workflowLines.Add(new WorkflowLine("分叉-中间", workflowId, workflowNodes[1].Id, workflowNodes[2].Id, new List<LineCondition>
            {
                new LineCondition(typeof(ConditionA).FullName,"ConditionA","x=1","testdes")
            }));
            workflowLines.Add(new WorkflowLine("分叉-中间1", workflowId, workflowNodes[1].Id, workflowNodes[3].Id, new List<LineCondition>
            {
                new LineCondition("ConditionA","ConditionA","x=2","testdes")
            }));

            workflowLines.Add(new WorkflowLine("中间-会签", workflowId, workflowNodes[2].Id, workflowNodes[4].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("中间1-会签", workflowId, workflowNodes[3].Id, workflowNodes[4].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("会签-结束", workflowId, workflowNodes[4].Id, workflowNodes[5].Id, new List<LineCondition>
            {
            }));


            workflowManager.UpdateWorkflow(workflowId.Id, "newname", "des", wf.ActiveVersion, string.Empty, string.Empty, workflowLines, workflowNodes).Wait();

            wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;

            Assert.IsTrue(wf.Name.Equals("newname"));

        }

        [Test]
        public void Approve()
        {
            UpdateWorkflow();

            var workflowManager = serviceProvider.GetService<WorkflowManager>();

            var workflowRepository = serviceProvider.GetService<IBasicRepository<Workflows.Workflow>>();
            var wf = workflowManager.GetWorkflowByNo("wfno").Result;

            WorkflowId workflowId = new WorkflowId(wf.ActiveVersion, wf.Id);
            var worktask = workflowManager.CreateWorkTask ( workflowId, "测试流程", JsonConvert.SerializeObject(new { x = 2, y = 1 }), "", "","").Result;

            var steps = workflowManager.WorkTaskStart(worktask.Id).Result;


            List<WorkStep> fxSteps = steps;

            //分叉节点审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "分叉节点审批通过", workflowManager, step.Id);
            }

            //分叉节点撤回
            foreach (var step in fxSteps)
            {
                steps = HandleStep(WorkStepHandleType.Withdraw, "分叉节点撤回", workflowManager, step.Id);
            }

            //分叉节点重新审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "分叉节点审批重新通过", workflowManager, step.Id);
            }

            //中间节点1审批
            HandleStep(WorkStepHandleType.Pass, "中间节点1审批", workflowManager, steps[0].Id);

            //中间节点1撤回
            steps = HandleStep(WorkStepHandleType.Withdraw, "中间节点1撤回", workflowManager, steps[0].Id);

            //中间节点1审批
            steps = HandleStep(WorkStepHandleType.Pass, "中间节点1重新审批", workflowManager, steps[0].Id);

            //会签节点审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "会签节点审批", workflowManager, step.Id);
            }

        

            //获取所有过程输出
            var historySteps = workflowManager.GetAllTaskStepsOfWorkTaskAsync(worktask.Id).Result;

            historySteps.ForEach(st =>
            {
                Console.WriteLine($"{st.HandlerTime}:{st.HandleUser.Name}::{st.HandleType}::{st.Comment}");
            });
        }

        private static List<WorkStep> HandleStep(WorkStepHandleType workStepHandleType, string comment, WorkflowManager workflowManager, Guid stepId)
        {
            List<WorkStep> worksteps = new List<WorkStep>();
            List<WorkStep> newsteps = null;
            if (workStepHandleType == WorkStepHandleType.Pass)
                newsteps = workflowManager.PassApprove(stepId, comment).Result.WorkSteps;
            else if (workStepHandleType == WorkStepHandleType.Reject)
                newsteps = workflowManager.RejectApprove(stepId, comment).Result.WorkSteps;
            else if (workStepHandleType == WorkStepHandleType.Withdraw)
                newsteps = workflowManager.Withdraw(stepId, comment).Result.WorkSteps;

            //step.Handle(workStepHandleType, comment);

            if (newsteps != null)
                worksteps.AddRange(newsteps);
            return worksteps;
        }
    }
}
