﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WorkFlowCore.Conditions;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Workflows
{

    public class LineCondition
    {
        public LineCondition()
        {
        }

        public LineCondition(string conditionId, string conditionName, string parameter, string description)
        {
            ConditionId = conditionId;
            ConditionName = conditionName;
            Parameter = parameter;
            Description = description;
        }

        /// <summary>
        /// 转换器id
        /// </summary>
        public string ConditionId { get; set; }
        /// <summary>
        /// 转换器名称
        /// </summary>
        public string ConditionName { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        [StringLength(1000)]
        public string Parameter { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [StringLength(500)]
        public string Description { get; set; }
    }
}
