﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Workflows
{
    public class RejectNode
    {
        public List<LineCondition> Conditions { get; set; }
        public Guid NodeId;
        public Guid NodeName;
    }
}
