﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Workflows
{
    public class Workflow : WithBaseInfoEntity
    {
        public Workflow(Guid id, string workflowNo, string name, string description)
        {
            Id = id;
            WorkflowNo = workflowNo;
            Name = name;
            ActiveVersion = 1;
            Description = description;
        }

        public Workflow()
        {
        }

        public void Update(string name, string description)
        {
            Name = name;
            Description = description;
        }
        /// <summary>
        /// 设置激活版本
        /// </summary>
        /// <param name="activeVersion"></param>
        public void UpdateActiveVersion(int activeVersion)
        {
            ActiveVersion = activeVersion;
        }


        /// <summary>
        /// 流程编号
        /// </summary>
        [Required]
        [StringLength(50)]
        public string WorkflowNo { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// 激活版本
        /// </summary>
        public int ActiveVersion { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
    }
}
