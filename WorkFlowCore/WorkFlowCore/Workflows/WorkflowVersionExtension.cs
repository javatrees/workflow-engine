﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Workflows
{
    public static class WorkflowVersionExtension
    {
        static WorkflowVersionExtension()
        {

        }
        public static WorkflowVersion ToWorkflowVersion(this WorkflowVersionInfo workflowVersionInfo)
        {
            var workflowVersion = new WorkflowVersion();

            workflowVersion.Id = workflowVersionInfo.Id;
            workflowVersion.WorkflowId = workflowVersionInfo.WorkflowId;
            workflowVersion.DrawingInfo = workflowVersionInfo.DrawingInfo;
            workflowVersion.VersionNo = workflowVersionInfo.VersionNo;
            workflowVersion.Description = workflowVersionInfo.Description;

            workflowVersion.ModifiedTime = workflowVersionInfo.ModifiedTime;
            workflowVersion.ModifiedUserId = workflowVersionInfo.ModifiedUserId;
            workflowVersion.CreationTime = workflowVersionInfo.CreationTime;
            workflowVersion.CreatedUserId = workflowVersionInfo.CreatedUserId;
            workflowVersion.Deleted = workflowVersionInfo.Deleted;
            workflowVersion.DeletedUserId = workflowVersionInfo.DeletedUserId;
            workflowVersion.DeletedTime = workflowVersionInfo.DeletedTime;

            if (!string.IsNullOrEmpty(workflowVersionInfo.NodeMaps))
            {
                workflowVersion.NodeMaps = JsonConvert.DeserializeObject<List<NodeMap>>(workflowVersionInfo.NodeMaps);
            }
            if (!string.IsNullOrEmpty(workflowVersionInfo.AllNodes))
            {
                workflowVersion.AllNodes = JsonConvert.DeserializeObject<List<WorkflowNode>>(workflowVersionInfo.AllNodes);
            }

            return workflowVersion;
        }

        public static WorkflowVersionInfo ToWorkflowVersionInfo(this WorkflowVersion workflowVersion, WorkflowVersionInfo workflowVersionInfo=null)
        {
            if(workflowVersionInfo==null)
                workflowVersionInfo = new WorkflowVersionInfo();

            workflowVersionInfo.Id = workflowVersion.Id;
            workflowVersionInfo.WorkflowId = workflowVersion.WorkflowId;
            workflowVersionInfo.DrawingInfo = workflowVersion.DrawingInfo;
            workflowVersionInfo.VersionNo = workflowVersion.VersionNo;
            workflowVersionInfo.Description = workflowVersion.Description;
            workflowVersionInfo.ModifiedTime = workflowVersion.ModifiedTime;
            workflowVersionInfo.ModifiedUserId = workflowVersion.ModifiedUserId;
            workflowVersionInfo.CreationTime = workflowVersion.CreationTime;
            workflowVersionInfo.CreatedUserId = workflowVersion.CreatedUserId;
            workflowVersionInfo.Deleted = workflowVersion.Deleted;
            workflowVersionInfo.DeletedUserId = workflowVersion.DeletedUserId;
            workflowVersionInfo.DeletedTime = workflowVersion.DeletedTime;

            if (workflowVersion.NodeMaps!=null)
            {
                workflowVersionInfo.NodeMaps = JsonConvert.SerializeObject(workflowVersion.NodeMaps);
            }
            if (workflowVersion.AllNodes != null)
            {
                workflowVersionInfo.AllNodes = JsonConvert.SerializeObject(workflowVersion.AllNodes);
            }

            return workflowVersionInfo;
        }
    }
}
