﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Workflows
{
    public class WorkflowId
    {
        public WorkflowId()
        {
        }

        public WorkflowId(int versionId, Guid id)
        {
            VersionId = versionId;
            Id = id;
        }

        /// <summary>
        /// 版本名称
        /// </summary>
        public int VersionId { get; set; }
        /// <summary>
        /// 流程编号
        /// </summary>
        public Guid Id { get; set; }
    }
}
