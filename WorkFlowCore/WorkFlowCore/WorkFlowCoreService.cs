﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using WorkFlowCore.Authorization;
using WorkFlowCore.Conditions;
using WorkFlowCore.IRepositories;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore
{
    public static class WorkFlowCoreService
    {
        public class WorkFlowCoreServiceConfig
        {
            public void RegisterSelector(params Assembly[] assemblies)
            {
                UserSelectorManager.RegisterSelector(assemblies);
            }
            public void Registercondition(params Assembly[] assemblies)
            {
                ConditionManager.Registercondition(assemblies);
            }

            public Type SessionType { get; private set; }
            /// <summary>
            /// 注册 session实现类
            /// </summary>
            /// <typeparam name="TSession"></typeparam>
            public void RegisterSession<TSession>() where TSession:IWorkflowSession
            {
                SessionType = typeof(TSession);
            }
        }
        public static void AddWorkFlowCore(this IServiceCollection services,Action<WorkFlowCoreServiceConfig> options=null)
        {
            services.AddScoped<UserSelectorManager>();
            services.AddScoped<ConditionManager>();

            services.AddScoped<Workflow>();
            services.AddScoped<WorkflowVersion>();
            services.AddScoped<WorkflowNode>();
            services.AddScoped<WorkflowLine>();
            services.AddScoped<WorkTask>();
            services.AddScoped<WorkStep>();
            services.AddScoped<WorkflowManager>();

            var config = new WorkFlowCoreServiceConfig();
            options?.Invoke(config);
            //如果前端注册了自定义session，则使用自定义的
            if(config.SessionType!=null)
                services.AddScoped(typeof(IWorkflowSession), config.SessionType);
            else
                services.AddScoped(typeof(IWorkflowSession), typeof(EmptySession));

        }
    }
}
