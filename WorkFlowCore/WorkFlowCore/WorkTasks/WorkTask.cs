﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.WorkTasks
{
    public class WorkTask: WithBaseInfoEntity
    {
        public WorkTask(Guid id,WorkflowId workflowId, string name, string formData, string entityFullName, string entityKeyValue, string createdUserId)
        {
            Id = id;
            WorkflowId = workflowId;
            Name = name;
            FormData = formData;
            EntityFullName = entityFullName;
            EntityKeyValue = entityKeyValue;
            CreatedUserId = createdUserId;
        }

        public WorkTask()
        {
        }

        /// <summary>
        /// 流程id
        /// </summary>
        public WorkflowId WorkflowId { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 表单数据（json）
        /// </summary>
        public string FormData { get; set; }
        /// <summary>
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
        /// <summary>
        /// 审批状态
        /// </summary>
        public WorkTaskStatus WorkTaskStatus { get; set; } = WorkTaskStatus.Pending;

        public void SetPending()
        {
            WorkTaskStatus = WorkTaskStatus.Pending;
        }
        public void SetProcessing()
        {
            WorkTaskStatus = WorkTaskStatus.Processing;
        }
        public void SetProcessed()
        {
            WorkTaskStatus = WorkTaskStatus.Processed;
        }
        public bool IsProcessed { get => WorkTaskStatus == WorkTaskStatus.Processed; }
        public bool IsPending { get => WorkTaskStatus == WorkTaskStatus.Pending; }
        public bool IsProcessing { get => WorkTaskStatus == WorkTaskStatus.Processing; }
        public bool IsSimulation { get; set; }
        public void AsSimulation()
        {
            IsSimulation = true;
        }
    }
    public enum WorkTaskStatus
    {
        /// <summary>
        /// 待处理
        /// </summary>
        Pending,
        /// <summary>
        /// 处理中
        /// </summary>
        Processing,
        /// <summary>
        /// 已完成
        /// </summary>
        Processed
    }
}
