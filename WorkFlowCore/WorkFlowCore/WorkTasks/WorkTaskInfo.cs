﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.WorkTasks
{
    public class WorkTaskInfo: WithBaseInfoEntity
    {
      

        /// <summary>
        /// 流程id
        /// </summary>
        public Guid WorkflowId_Id { get; set; }
        public int WorkflowId_VersionId { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 表单数据（json）
        /// </summary>
        public string FormData { get; set; }
        /// <summary>
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
        /// <summary>
        /// 审批状态
        /// </summary>
        public WorkTaskStatus WorkTaskStatus { get; set; }
        public bool IsSimulation { get; set; }
    }
    
}
