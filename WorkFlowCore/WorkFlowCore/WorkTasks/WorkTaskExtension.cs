﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.WorkTasks
{
    public static class WorkTaskExtension
    {
        public static WorkTask ToWorkTask(this WorkTaskInfo workTaskInfo)
        {
            var workTask = new WorkTask();
            workTask.CreationTime = workTaskInfo.CreationTime;
            workTask.CreatedUserId = workTaskInfo.CreatedUserId;
            workTask.Deleted = workTaskInfo.Deleted;
            workTask.DeletedUserId = workTaskInfo.DeletedUserId;
            workTask.DeletedTime = workTaskInfo.DeletedTime;
            workTask.EntityFullName = workTaskInfo.EntityFullName;
            workTask.EntityKeyValue = workTaskInfo.EntityKeyValue;
            workTask.FormData = workTaskInfo.FormData;
            workTask.Id = workTaskInfo.Id;
            workTask.ModifiedTime = workTaskInfo.ModifiedTime;
            workTask.ModifiedUserId = workTaskInfo.ModifiedUserId;
            workTask.Name = workTaskInfo.Name;
            workTask.WorkflowId = new Workflows.WorkflowId(workTaskInfo.WorkflowId_VersionId, workTaskInfo.WorkflowId_Id);
            workTask.WorkTaskStatus = workTaskInfo.WorkTaskStatus;
            workTask.IsSimulation = workTaskInfo.IsSimulation;


            return workTask;
        }

        public static WorkTaskInfo ToWorkTaskInfo(this WorkTask workTask, WorkTaskInfo workTaskInfo=null)
        {
            if(workTaskInfo==null)
                workTaskInfo = new WorkTaskInfo();
            workTaskInfo.CreationTime = workTask.CreationTime;
            workTaskInfo.CreatedUserId = workTask.CreatedUserId;
            workTaskInfo.Deleted = workTask.Deleted;
            workTaskInfo.DeletedUserId = workTask.DeletedUserId;
            workTaskInfo.DeletedTime = workTask.DeletedTime;
            workTaskInfo.EntityFullName = workTask.EntityFullName;
            workTaskInfo.EntityKeyValue = workTask.EntityKeyValue;
            workTaskInfo.FormData = workTask.FormData;
            workTaskInfo.Id = workTask.Id;
            workTaskInfo.ModifiedTime = workTask.ModifiedTime;
            workTaskInfo.ModifiedUserId = workTask.ModifiedUserId;
            workTaskInfo.Name = workTask.Name;
            workTaskInfo.WorkflowId_Id = workTask.WorkflowId.Id;
            workTaskInfo.WorkflowId_VersionId = workTask.WorkflowId.VersionId;
            workTaskInfo.WorkTaskStatus = workTask.WorkTaskStatus;
            workTaskInfo.IsSimulation = workTask.IsSimulation;

            return workTaskInfo;
        }
    }
}
