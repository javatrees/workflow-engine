﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkFlowCore.IRepositories
{
    public interface IWithBaseInfoEntity<TKey> : IEntity<TKey>
    {
        /// <summary>
        /// 更新时间
        /// </summary>
        DateTime ModifiedTime { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        string ModifiedUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        string CreatedUserId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime CreationTime { get; set; }

        /// <summary>
        /// 删除用户
        /// </summary>
        string DeletedUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        DateTime DeletedTime { get; set; }
        /// <summary>
        /// 删除状态
        /// </summary>
        bool Deleted { get; set; }
    }

    public interface IWithBaseInfoEntity : IWithBaseInfoEntity<Guid>
    {
    }


}
