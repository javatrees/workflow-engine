﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkFlowCore.IRepositories
{

    public class WithBaseInfoEntity<TKey> : IWithBaseInfoEntity<TKey>
    {
        public DateTime ModifiedTime { get; set; }
        public string ModifiedUserId { get; set; }
        public string CreatedUserId { get; set; }
        public DateTime CreationTime { get; set; }

        public TKey Id { get; set; }
        public string DeletedUserId { get; set; }
        public DateTime DeletedTime { get; set; }
        public bool Deleted { get; set; } = false;

        public object[] GetKeys()
        {
            return GetType().GetProperties().Select(p => p.Name).ToArray();
        }
    }
    public class WithBaseInfoEntity : WithBaseInfoEntity<Guid> { }
}
