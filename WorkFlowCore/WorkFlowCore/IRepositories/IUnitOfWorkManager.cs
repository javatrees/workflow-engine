﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.IRepositories
{
    public interface IUnitOfWorkManager
    {
        IUnitOfWork Begin();
    }
}
