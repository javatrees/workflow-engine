﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.IRepositories
{
    public interface IWorkStepRepository : IBasicRepository<WorkStepInfo, Guid>
    {
        Task<PageResult<WorkStep>> GetUnHandlerWorkStepsOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
    }
}
