﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.IRepositories
{
    public interface IUnitOfWork:IDisposable
    {
       
        bool Commit();
        bool IsActive();
    }

    public static class IUnitOfWorkExtension
    {
        public static T Commit<T>(this IUnitOfWork unitOfWork, T succeedResult, T failedResult)
        {
            return unitOfWork.Commit() ? succeedResult : failedResult;
        }
    }
}
