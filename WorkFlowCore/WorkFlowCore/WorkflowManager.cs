﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Conditions;
using WorkFlowCore.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.IRepositories;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore
{
    public class WorkflowManager
    {
        private readonly IBasicRepository<Workflow, Guid> workflowRepository;
        private readonly IBasicRepository<WorkflowVersionInfo, Guid> versionRepository;
        private readonly IWorkTaskRepository workTaskRepository;
        private readonly IWorkStepRepository workStepRepository;
        private readonly ConditionManager conditionManager;
        private readonly UserSelectorManager userSelectorManager;
        private readonly EventBusManager eventManager;
        private readonly IUnitOfWorkManager unitOfWorkManager;

        public WorkflowManager(IBasicRepository<Workflow, Guid> workflowRepository, IBasicRepository<WorkflowVersionInfo, Guid> versionRepository, IWorkTaskRepository workTaskRepository, IWorkStepRepository workStepRepository, ConditionManager conditionManager, UserSelectorManager userSelectorManager, EventBusManager eventManager, IUnitOfWorkManager unitOfWorkManager)
        {
            this.workflowRepository = workflowRepository;
            this.versionRepository = versionRepository;
            this.workTaskRepository = workTaskRepository;
            this.workStepRepository = workStepRepository;
            this.conditionManager = conditionManager;
            this.userSelectorManager = userSelectorManager;
            this.eventManager = eventManager;
            this.unitOfWorkManager = unitOfWorkManager;
        }


        #region 流程的增删改查

        public async Task<List<Workflow>> GetAllWorkflowsWithVersion()
        {
            var result = new List<Workflow>();
            var workflows = await workflowRepository.GetListAsync(w => !w.Deleted);
            workflows.ForEach(w =>
            {
                var versions = versionRepository.GetListAsync(v => v.WorkflowId == w.Id).Result;
                result.AddRange(versions.Select(v => new Workflow
                {
                    Id = w.Id,
                    WorkflowNo = w.WorkflowNo,
                    Name = w.Name,
                    ActiveVersion = v.VersionNo
                }));
            });
            return result;
        }

        /// <summary>
        /// 根据编号获取流程设计
        /// </summary>
        /// <param name="workflowNo"></param>
        /// <returns></returns>
        public async Task<Workflow> GetWorkflowByNo(string workflowNo)
        {
            return await workflowRepository.GetAsync(w => w.WorkflowNo == workflowNo);
        }

        public async Task<Workflow> CreateWorkflow(string workflowNo, string name, string description)
        {
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                var workflow = new Workflow(Guid.NewGuid(), workflowNo, name, description);
                workflow = await workflowRepository.InsertAsync(workflow);
                var workflowVersion = new WorkflowVersion(Guid.NewGuid(), workflow.Id, workflow.ActiveVersion, string.Empty, description);
                await versionRepository.InsertAsync(workflowVersion.ToWorkflowVersionInfo());
                unitOfWork.Commit();
                return workflow;
            }
        }
        /// <summary>
        /// 更新流程激活版本
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="activeVersion"></param>
        public async Task<bool> UpdateWorkflowActiveVersion(Guid workflowId, int activeVersion)
        {
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                var workflow = await workflowRepository.GetAsync(workflowId);
                workflow.UpdateActiveVersion(activeVersion);
                await workflowRepository.UpdateAsync(workflow);
                return unitOfWork.Commit(true, false);
            }
        }
        /// <summary>
        /// 删除流程
        /// </summary>
        /// <param name="workflowid"></param>
        /// <returns></returns>
        public async Task<bool> DeleteWorkflow(Guid workflowid)
        {

            var tasks = await workflowRepository.GetListAsync(wt => wt.Id == workflowid);
            tasks.ForEach(t => t.Deleted = true);

            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workflowRepository.UpdateManyAsync(tasks);
                return unitOfWork.Commit(true, false);
            }
        }
        /// <summary>
        /// 删除流程版本
        /// </summary>
        /// <param name="workflowid"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteWorkflowVersion(Guid workflowid, int versionId)
        {

            //有被流程引用则不允许删除
            if (await workTaskRepository.GetCountAsync(wt => wt.WorkflowId_Id == workflowid && wt.WorkflowId_VersionId == versionId) > 0) return false;
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await versionRepository.DeleteManyAsync(wt => wt.WorkflowId == workflowid && wt.VersionNo == versionId);
                return unitOfWork.Commit(true, false);
            }
        }

        private List<NodeMap> GetNewNodeMaps(List<WorkflowLine> workflowLines, List<WorkflowNode> workflowNodes)
        {
            var result = new List<NodeMap>();
            workflowLines.ForEach(line =>
            {
                var fromNode = workflowNodes.FirstOrDefault(n => n.Id == line.FromNodeId);
                var toNode = workflowNodes.FirstOrDefault(n => n.Id == line.ToNodeId);

                result.Add(NodeMap.Factory.NewNormalNodeMap(fromNode, toNode, line.Conditions));

                //如果指定拒绝节点，则按照拒绝节点配置拒绝映射，否则原路拒绝
                if (toNode.RejectNodes != null && toNode.RejectNodes.Any())
                {
                    toNode.RejectNodes.ForEach(rn =>
                    {
                        var rejectFromNode = toNode;
                        var rejectToNode = workflowNodes.FirstOrDefault(n => n.Id == rn.NodeId);

                        result.Add(NodeMap.Factory.NewRejectNodeMap(rejectFromNode, rejectToNode, rn.Conditions));
                    });
                }
                else
                {
                    //判断起始节点是否是开始或会签节点，是的话，直接退回，不是的话，获取来所有以起始节点为终点的所有节点条件放入拒绝判断列表中
                    if (fromNode.NodeType == WorkNodeType.Begin || fromNode.NodeType == WorkNodeType.Sign || fromNode.NodeType == WorkNodeType.Judge || fromNode.NodeType == WorkNodeType.HandOut)
                    {
                        result.Add(NodeMap.Factory.NewRejectNodeMap(toNode, fromNode, null));
                    }
                    else
                    {
                        var fromLines = workflowLines.Where(line => line.ToNodeId == fromNode.Id).ToList();
                        fromLines.ForEach(line =>
                        {
                            result.Add(NodeMap.Factory.NewRejectNodeMap(toNode, fromNode, line.Conditions));
                        });
                    }
                }
            });
            return result;
        }

        private async Task<WorkflowVersionInfo> GetWorkflowVersionInfo(Guid workflowId, int versionNo)
        {
            return await versionRepository.GetAsync(v => v.WorkflowId == workflowId && v.VersionNo == versionNo);
        }
        public async Task<WorkflowVersion> GetWorkflowVersion(Guid workflowId, int versionNo)
        {
            return (await GetWorkflowVersionInfo(workflowId, versionNo)).ToWorkflowVersion();
        }

        /// <summary>
        /// 更新流程设计
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="versionNo"></param>
        /// <param name="drawingInfo"></param>
        /// <param name="versionDescription"></param>
        /// <param name="workflowLines"></param>
        /// <param name="workflowNodes"></param>
        /// <returns></returns>

        public async Task<Workflow> UpdateWorkflow(Guid workflowId, string name, string description, int versionNo, string drawingInfo, string versionDescription, List<WorkflowLine> workflowLines, List<WorkflowNode> workflowNodes)
        {
            var workflow = await workflowRepository.GetAsync(workflowId);
            workflow.Update(name, description);
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workflowRepository.UpdateAsync(workflow);
                WorkflowVersion version = null;
                var versionInfo = await GetWorkflowVersionInfo(workflowId, versionNo);
                if (versionInfo == null)
                {
                    //新建版本
                    version = new WorkflowVersion(Guid.NewGuid(), workflowId, versionNo, drawingInfo, versionDescription);
                    version.SetNodeMaps(GetNewNodeMaps(workflowLines, workflowNodes));
                    await versionRepository.InsertAsync(version.ToWorkflowVersionInfo());
                }
                else
                {
                    version = versionInfo.ToWorkflowVersion();
                    version.SetNodeMaps(GetNewNodeMaps(workflowLines, workflowNodes));
                    version.Update(versionNo, drawingInfo, versionDescription);
                    await versionRepository.UpdateAsync(version.ToWorkflowVersionInfo(versionInfo));
                }

                return unitOfWork.Commit(workflow, null);
            }
        }

        #endregion

        #region 流程审批操作

        /// <summary>
        /// 创建流程
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="name"></param>
        /// <param name="formData"></param>
        /// <param name="entityFullName"></param>
        /// <param name="entityKeyValue"></param>
        /// <returns></returns>
        public async Task<WorkTask> CreateWorkTask(WorkflowId workflowId, string name, string formData, string entityFullName, string entityKeyValue, string createdUserId)
        {

            var worktaskInfo = await workTaskRepository.GetAsync(w => w.EntityFullName == entityFullName && w.EntityKeyValue == entityKeyValue && w.WorkflowId_Id == workflowId.Id && w.WorkflowId_VersionId == workflowId.VersionId);

            if (worktaskInfo == null)
            {
                var worktask = new WorkTask(Guid.NewGuid(), workflowId, name, formData, entityFullName, entityKeyValue, createdUserId);
                worktaskInfo = worktask.ToWorkTaskInfo();
                using (var unitOfWork = unitOfWorkManager.Begin())
                {
                    await workTaskRepository.InsertAsync(worktaskInfo);
                    unitOfWork.Commit();
                }
            }
            return worktaskInfo.ToWorkTask();
        }

        /// <summary>
        /// 创建模拟流程
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="name"></param>
        /// <param name="formData"></param>
        /// <param name="entityFullName"></param>
        /// <param name="entityKeyValue"></param>
        /// <returns></returns>
        public async Task<WorkTask> CreateSimulationWorkTask(WorkflowId workflowId, string name, string formData, string entityFullName, string entityKeyValue, string createdUserId)
        {

            var worktask = new WorkTask(Guid.NewGuid(), workflowId, name, formData, entityFullName, entityKeyValue, createdUserId);
            worktask.AsSimulation();
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workTaskRepository.InsertAsync(worktask.ToWorkTaskInfo());
                unitOfWork.Commit();
            }
            return worktask;
        }


        private async Task<WorkflowNode> GetNodeByWorkflowIdAndNodeId(WorkflowId workflowId, Guid nodeId)
        {
            var workflowVersion = await GetWorkflowVersion(workflowId.Id, workflowId.VersionId);
            return workflowVersion.AllNodes.FirstOrDefault(n => n.Id == nodeId);
        }


        /// <summary>
        /// 获取审批的下一步节点
        /// </summary>
        /// <param name="node"></param>
        /// <param name="workTask"></param>
        /// <returns></returns>
        private async Task<List<WorkflowNode>> GetNextNodes(WorkflowNode node, WorkTask workTask, WorkStep cuttentWorkStep)
        {
            var nodes = new List<WorkflowNode>();

            var workflowVersion = await GetWorkflowVersion(workTask.WorkflowId.Id, workTask.WorkflowId.VersionId);

            var toNodeLines = workflowVersion.NodeMaps.Where(n => n.FromNode.Id == node.Id && n.MapType == NodeMap.NodeMapType.Normal);

            foreach (var line in toNodeLines)
            {
                if (line.CanAccept(workTask, cuttentWorkStep, conditionManager))
                {
                    var tonode = line.ToNode;
                    nodes.Add(tonode);
                }
            }
            return nodes;
        }



        private async Task<Tuple<List<WorkStep>, List<WorkflowNode>>> GetRejectInfo(WorkflowNode node, WorkTask workTask, WorkStep currentWorkStep)
        {
            /*
             * 如果指定了拒绝返回的节点，则按指定节点返回
             * 如果没指定返回节点，则判断来源节点是不是拒绝步骤，
             *  是   根据流程节点关系推断退回节点
             *  否   根据审批记录退回到上一个节点
             */

            var workflowVersion = await GetWorkflowVersion(workTask.WorkflowId.Id, workTask.WorkflowId.VersionId);
            var preSteps = (await workStepRepository.GetListAsync(ws => ws.GroupId == currentWorkStep.PreStepGroupId)).Select(s => s.ToWorkStep()).ToList();
            preSteps = preSteps.GroupBy(s => s.NodeId + s.HandleUser.ToString()).Select(g => g.OrderByDescending(step => step.CreationTime).First()).ToList();

            var rejectNodes = new List<WorkflowNode>();
            var groupId = Guid.NewGuid().ToString();
            var steps = new List<WorkStep>();
            if (node.RejectNodes != null && node.RejectNodes.Any())
            {
                //如果指定了回滚的节点，则按指定节点回滚
                foreach (var rejectNode in node.RejectNodes)
                {
                    var toNode = workflowVersion.AllNodes.FirstOrDefault(n => n.Id == rejectNode.NodeId);
                    rejectNodes.Add(toNode);
                    steps.AddRange(GetApproveSteps(currentWorkStep.NodeId, node.Name, workTask, toNode, currentWorkStep.GroupId, groupId));
                }
            }
            else if (preSteps.Where(ps => ps.HandleType == WorkStepHandleType.Reject).Any())
            {
                var toNodeLines = workflowVersion.NodeMaps.Where(n => n.FromNode.Id == node.Id && n.MapType == NodeMap.NodeMapType.Reject);

                foreach (var line in toNodeLines)
                {
                    if (line.CanAccept(workTask, currentWorkStep, conditionManager))
                    {
                        var toNode = line.ToNode;
                        rejectNodes.Add(toNode);
                        steps.AddRange(GetApproveSteps(currentWorkStep.NodeId, node.Name, workTask, toNode, currentWorkStep.GroupId, groupId));
                    }
                }
            }
            else
            {

                preSteps.ForEach(st =>
                {
                    var toNode = workflowVersion.AllNodes.FirstOrDefault(n => n.Id == st.NodeId);
                    rejectNodes.Add(toNode);
                    steps.Add(st.Copy(groupId));
                });
            }



            return Tuple.Create(steps, rejectNodes);
        }


        /// <summary>
        /// 获取节点派发的成员
        /// </summary>
        /// <param name="node"></param>
        /// <param name="workTask"></param>
        /// <returns></returns>
        public Dictionary<NodeUser.NodeHandleType, List<User>> GetNodeUsers(WorkflowNode node, WorkTask workTask)
        {
            return node.GetHandleUsers(workTask, userSelectorManager);
        }


        /// <summary>
        /// 更新同组其它审批步骤状态为未处理
        /// </summary>
        /// <param name="currentNodeId"></param>
        /// <param name="stepGroupId"></param>
        /// <returns></returns>
        private async Task UpdateOtherStepsStatusWithUnWork(Guid currentNodeId, string stepGroupId)
        {
            await Task.CompletedTask;
            var stepInfos = new List<WorkStepInfo>();
            //更新步骤所包含的节点状态为 未处理状态
            var steps = await workStepRepository.GetListAsync(s => s.NodeId != currentNodeId && s.GroupId == stepGroupId);
            foreach (var stepInfo in steps)
            {
                //对于未处理的记录，更新其处理状态
                if (!stepInfo.IsHandled)
                {
                    var step = stepInfo.ToWorkStep();
                    step.Handle(WorkStepHandleType.UnWork);
                    stepInfos.Add(step.ToWorkStepInfo(stepInfo));
                }
            }
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workStepRepository.UpdateManyAsync(stepInfos);
                unitOfWork.Commit();
            }
        }
        /// <summary>
        /// 更新同节点其它用户步骤状态为未处理
        /// 更新当前步骤所在节点的其它审批步骤为  未处理 状态
        /// </summary>
        /// <param name="currentWorkStep"></param>
        /// <returns></returns>
        private async Task UpdateOtherStepsStatusWithUnWorkOfNode(WorkStep currentWorkStep)
        {
            await Task.CompletedTask;
            var stepInfos = new List<WorkStepInfo>();
            var steps = await workStepRepository.GetListAsync(ws => ws.NodeId == currentWorkStep.NodeId && ws.GroupId == currentWorkStep.GroupId && ws.Id != currentWorkStep.Id && !ws.IsHandled);
            foreach (var stepInfo in steps)
            {
                //对于未处理的记录，更新其处理状态
                if (!stepInfo.IsHandled)
                {
                    var step = stepInfo.ToWorkStep();
                    step.Handle(WorkStepHandleType.UnWork);
                    stepInfos.Add(step.ToWorkStepInfo(stepInfo));
                }
            }
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workStepRepository.UpdateManyAsync(stepInfos);
                unitOfWork.Commit();
            }
        }




        /// <summary>
        /// 发起审批
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public async Task<List<WorkStep>> WorkTaskStart(Guid workTaskId)
        {
            var workTaskInfo = await workTaskRepository.GetAsync(workTaskId);
            if (workTaskInfo == null) return new List<WorkStep>();
            var workTask = workTaskInfo.ToWorkTask();
            if (!workTask.IsPending) return new List<WorkStep>();
            workTask.SetProcessing();
            var workflowVersion = await GetWorkflowVersion(workTask.WorkflowId.Id, workTask.WorkflowId.VersionId);
            var startNode = workflowVersion.NodeMaps.FirstOrDefault(n => n.FromNode.NodeType == WorkNodeType.Begin)?.FromNode;

            var steps = GetApproveSteps(Guid.Empty, "", workTask, startNode, string.Empty, Guid.NewGuid().ToString());
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                await workTaskRepository.UpdateAsync(workTask.ToWorkTaskInfo(workTaskInfo));
                await SendTasks(workTask, steps, workTask.FormData);
                unitOfWork.Commit();
            }

            //自动处理开始节点


            /*
             * 这里单独开事务有两个考虑的地方：
             * 1.就算这里没自动处理成功，变成人工审批处理也是不影响流程
             * 2.能力有限，默认实现的简易数据持久化仓储以及工作单元无法支持复杂的事务嵌套操作
             */
            var startSteps = new List<WorkStep>();
            foreach (var step in steps)
            {
                var _steps = (await PassApprove(step.Id, "发起审批", string.Empty)).WorkSteps;
                if (_steps != null)
                    startSteps.AddRange(_steps);
            }
            steps = startSteps;

            return steps;
        }

        /// <summary>
        /// 判断当前节点所有人员是否都处理完了
        /// </summary>
        /// <param name="currentWorkStep"></param>
        /// <param name="workTask"></param>
        /// <returns></returns>
        private async Task<bool> HasAllHandlersWorkflowed(WorkStep currentWorkStep, WorkTask workTask)
        {
            await Task.CompletedTask;
            var GetCountAsync = await workStepRepository.GetCountAsync(ws => ws.GroupId == currentWorkStep.GroupId && ws.Id != currentWorkStep.Id && !ws.IsHandled);
            return GetCountAsync == 0;
        }

        /// <summary>
        /// 通过审批
        /// </summary>
        /// <param name="workStepId"></param>
        /// <param name="comment"></param>
        /// <param name="resourceIds"></param>
        /// <param name="UserSelectors"></param>
        /// <returns></returns>
        public async Task<ProveResult> PassApprove(Guid workStepId, string comment = null, string resourceIds = null, string formData = null, List<NodeUser> userSelectors = null)
        {
            var currentWorkStepInfo = await workStepRepository.GetAsync(workStepId);
            var currentWorkStep = currentWorkStepInfo.ToWorkStep();

            if (currentWorkStep.WorkStepType == WorkStepType.ReadOnly)
                return ProveResult.Failed("只读步骤无法处理！");

            if (currentWorkStep.IsHandled)
                return ProveResult.Failed("步骤已处理！");




            //派发下个节点任务
            var workTaskInfo = await workTaskRepository.GetAsync(currentWorkStep.WorkTaskId);
            var workTask = workTaskInfo.ToWorkTask();
            var currentNode = await GetNodeByWorkflowIdAndNodeId(workTask.WorkflowId, currentWorkStep.NodeId);
            var steps = new List<WorkStep>();
            var nextNodes = await GetNextNodes(currentNode, workTask, currentWorkStep);

            //更新当前处理节点为已处理
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                currentWorkStep.Handle(WorkStepHandleType.Pass, comment, resourceIds);
                await workStepRepository.UpdateAsync(currentWorkStep.ToWorkStepInfo(currentWorkStepInfo));



                if (currentNode.IsWaitingAllUser)
                {
                    lock ("WorkNodeType.Sign")
                    {
                        if (!HasAllHandlersWorkflowed(currentWorkStep, workTask).Result)
                        {
                            return ProveResult.Succeed(steps);
                        }
                    }
                }
                else
                {
                    await UpdateOtherStepsStatusWithUnWorkOfNode(currentWorkStep);
                }
                //如果是会签操作,则确保所有会签中的操作都完成了才进行下一步
                if (nextNodes.Count == 1 && nextNodes[0].NodeType == WorkNodeType.Sign)
                {
                    //会签
                    //等待所有待处理的处理才继续
                    lock ("WorkNodeType.Sign")
                    {
                        if (GetOnHandingSignStepsCount(currentWorkStep.NodeId, currentWorkStep.GroupId).Result != 0)
                        {
                            return ProveResult.Succeed(steps); ;
                        }
                    }
                }
                var nextGroupId = Guid.NewGuid().ToString();


                foreach (var node in nextNodes)
                {
                    //如果 指定了处理人，则直接派给处理人
                    //如果指定了 抄送（只读）人员，直接推送给抄送人员
                    if (userSelectors != null && userSelectors.Any())
                    {
                        GetUsersByUserSelectors(userSelectors, null, (selector, user) =>
                        {
                            steps.Add(new WorkStep(Guid.NewGuid(), workTask.Id, currentWorkStep.NodeId, currentNode.Name, node.Id, node.Name, new User
                            {
                                Id = user.Id,
                                Name = user.Name
                            }, selector.HandleType == NodeUser.NodeHandleType.Handle ? WorkStepType.Handle : WorkStepType.ReadOnly, currentWorkStep.GroupId, nextGroupId));
                        });
                    }
                    else
                    {
                        steps.AddRange(GetApproveSteps(currentWorkStep.NodeId, currentNode.Name, workTask, node, currentWorkStep.GroupId, nextGroupId));
                    }
                }



                if (steps.Count == 0 && currentNode.NodeType != WorkNodeType.End)
                    return ProveResult.Failed("找不到可以处理的下一个步骤！");
                await SendTasks(workTask, steps, formData);
                if (!unitOfWork.Commit())
                    ProveResult.Failed("提交失败");

                //检查更新流程状态为审判中或者结束
                await CheckAndSetTaskProcessing(workTask, workTaskInfo, currentNode);
                await CheckAndSetTaskProcessed(workTask, workTaskInfo, currentNode);
            }
            //自动处理结束节点
            if (nextNodes.Count == 1 && nextNodes[0].NodeType == WorkNodeType.End)
            {
                /*
                 * 这里单独开事务有两个考虑的地方：
                 * 1.就算这里没自动处理成功，变成人工审批处理也是不影响流程
                 * 2.默认实现的简易数据持久化仓储以及工作单元无法支持复杂的事务嵌套操作
                 */
                var startSteps = new List<WorkStep>();
                foreach (var step in steps)
                {
                    var _steps = (await PassApprove(step.Id, "审批结束", string.Empty)).WorkSteps;
                    if (_steps != null)
                        startSteps.AddRange(_steps);
                }
                steps = startSteps;
            }


            return ProveResult.Succeed(steps);
        }

        /// <summary>
        /// 驳回审批
        /// </summary>
        /// <param name="workTask"></param>
        /// <param name="currentNodeId"></param>
        /// <returns></returns>
        public async Task<ProveResult> RejectApprove(Guid workStepId, string comment = null, string resourceIds = null, string formData = null)
        {
            var currentWorkStepInfo = await workStepRepository.GetAsync(workStepId);
            var currentWorkStep = currentWorkStepInfo.ToWorkStep();

            if (currentWorkStep.WorkStepType == WorkStepType.ReadOnly)
                return ProveResult.Failed("只读步骤无法处理！");

            if (currentWorkStep.IsHandled)
                return ProveResult.Failed("步骤已处理！");


            var workTaskInfo = await workTaskRepository.GetAsync(currentWorkStep.WorkTaskId);
            var workTask = workTaskInfo.ToWorkTask();

            //将任务驳回上一步骤
            var currentNode = await GetNodeByWorkflowIdAndNodeId(workTask.WorkflowId, currentWorkStep.NodeId);

            if (currentNode.NodeType == WorkNodeType.Begin)
                return ProveResult.Failed("开始步骤无法拒绝！");

            //更新当前处理节点为已处理
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                currentWorkStep.Handle(WorkStepHandleType.Reject, comment, resourceIds);
                await workStepRepository.UpdateAsync(currentWorkStep.ToWorkStepInfo(currentWorkStepInfo));
                var steps = new List<WorkStep>();

                var nextNodes = await GetNextNodes(currentNode, workTask, currentWorkStep);
                //如果下一个节点是会签，则同组的步骤都要撤回
                if (nextNodes.Count == 1 && nextNodes[0].NodeType == WorkNodeType.Sign)
                {
                    await UpdateOtherStepsStatusWithUnWork(currentWorkStep.NodeId, currentWorkStep.GroupId);
                }


                //获取拒绝的步骤（如果指定绝节节点则获取拒绝节点，否则退回上一步的节点，处理人员仍然 是上一步的人员）
                var rejectInfo = await GetRejectInfo(currentNode, workTask, currentWorkStep);
                var rejectSteps = rejectInfo.Item1;
                steps.AddRange(rejectSteps);
                if (steps.Count == 0)
                    return ProveResult.Failed("找不到可以处理的下一个步骤！");

                //如果拒绝返回的节点是开始 节点，则更新流程状态为待审批
                var rejectNodes = rejectInfo.Item2;
                var startNode = rejectNodes.FirstOrDefault(n => n.NodeType == WorkNodeType.Begin);
                if (startNode != null)
                {
                    await CheckAndTaskPendding(workTask, workTaskInfo, startNode);
                }

                await SendTasks(workTask, steps, formData);

                return unitOfWork.Commit(ProveResult.Succeed(steps), ProveResult.Failed("提交失败"));
            }
        }


        /// <summary>
        /// 撤回
        /// 只能撤回未读的记录
        /// </summary>
        /// <param name="workStepId"></param>
        /// <returns></returns>
        public async Task<ProveResult> Withdraw(Guid workStepId, string comment = null)
        {
            await Task.CompletedTask;

            var currentWorkStepInfo = await workStepRepository.GetAsync(workStepId);
            var currentWorkStep = currentWorkStepInfo.ToWorkStep();
            if (currentWorkStep.WorkStepType == WorkStepType.ReadOnly)
                return ProveResult.Failed("只读步骤无法处理！");

            if (!currentWorkStep.IsHandled)
                return ProveResult.Failed("步骤未处理无法撤回！");

            var workTaskInfo = await workTaskRepository.GetAsync(currentWorkStep.WorkTaskId);
            var workTask = workTaskInfo.ToWorkTask();
            if (workTask.IsProcessed)
                return ProveResult.Failed("流程已结束无法撤回！");

            var currentNode = await GetNodeByWorkflowIdAndNodeId(workTask.WorkflowId, currentWorkStep.NodeId);

            if (currentNode.NodeType == WorkNodeType.End)
                return ProveResult.Failed("已结束无法撤回！");

            var nextNodes = await GetNextNodes(currentNode, workTask, currentWorkStep);

            var steps = new List<WorkStep>();

            var nextSteps = await workStepRepository.GetListAsync(ws => ws.PreStepGroupId == currentWorkStep.GroupId && ws.HandleType != WorkStepHandleType.UnWork);
            //如果要撤回的节点是转发出去了，则全都要撤回
            if (currentWorkStep.HandleType == WorkStepHandleType.Forward)
            {
                nextSteps.AddRange(await GetForwardSteps(currentWorkStep));
            }


            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                //如果已读则不处理，否则执行撤回||如果下一个节点是会签节点，则允许撤回当前的操作重新审批
                if ((nextSteps.Any() && !nextSteps.Where(s => s.IsRead || s.IsHandled).Any()) || (nextNodes.Count == 1 && nextNodes[0].NodeType == WorkNodeType.Sign))
                {
                    //设置后续节点为未处理
                    var stepInfos = new List<WorkStepInfo>();
                    foreach (var nextStepsInfo in nextSteps)
                    {
                        var nextStep = nextStepsInfo.ToWorkStep();
                        nextStep.Handle(WorkStepHandleType.UnWork, $"撤回：{comment}");
                        stepInfos.Add(nextStep.ToWorkStepInfo(nextStepsInfo));
                    }
                    await workStepRepository.UpdateManyAsync(stepInfos);
                    //设置当前节点为撤回状态
                    //新插入当前步骤
                    var withdrawStep = currentWorkStep.Copy();
                    withdrawStep.Handle(WorkStepHandleType.Withdraw, comment);
                    await workStepRepository.InsertAsync(withdrawStep.ToWorkStepInfo());
                    //新插入当前步骤
                    var newStep = currentWorkStep.Copy();
                    newStep.SetReaded();//对于撤回新插入的记录，它应该是已读的
                    await workStepRepository.InsertAsync(newStep.ToWorkStepInfo());
                    steps.Add(newStep);
                }
                else
                {
                    return ProveResult.Failed("下一步骤已读或已处理，无法撤回！");
                }
                await CheckAndTaskPendding(workTask, workTaskInfo, currentNode);

                if (steps.Count == 0)
                    return ProveResult.Failed("找不到可以处理的下一个步骤！");
                return unitOfWork.Commit(ProveResult.Succeed(steps), ProveResult.Failed("提交失败"));
            }
        }
        /// <summary>
        /// 获取转发出去的步骤id
        /// </summary>
        /// <param name="currentWorkStep"></param>
        /// <returns></returns>
        private async Task<IEnumerable<WorkStepInfo>> GetForwardSteps(WorkStep currentWorkStep)
        {
            return await workStepRepository.GetListAsync(ws => ws.FromForwardStepId == currentWorkStep.Id);
        }

        /// <summary>
        /// 检查并重置流程状态
        /// </summary>
        /// <param name="workTask"></param>
        /// /// <param name="workTaskInfo"></param>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        private async Task CheckAndTaskPendding(WorkTask workTask, WorkTaskInfo workTaskInfo, WorkflowNode currentNode)
        {
            ///如果当前节点是开始节点，则流程状态更新为待处理
            if (currentNode.NodeType == WorkNodeType.Begin)
            {
                workTask.SetPending();
                await workTaskRepository.UpdateAsync(workTask.ToWorkTaskInfo(workTaskInfo));
                eventManager.Trigger(new TaskStateChangeEventData
                {
                    WorkTask = workTask,
                    WorkTaskStatus = workTask.WorkTaskStatus,
                });

            }
        }

        /// <summary>
        /// 检查并重置流程状态
        /// </summary>
        /// <param name="workTask"></param>
        /// /// <param name="workTaskInfo"></param>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        private async Task CheckAndSetTaskProcessing(WorkTask workTask, WorkTaskInfo workTaskInfo, WorkflowNode preNode)
        {
            ///如果当前节点是开始节点，则流程状态更新为待处理
            if (preNode.NodeType == WorkNodeType.Begin)
            {
                workTask.SetProcessing();
                await workTaskRepository.UpdateAsync(workTask.ToWorkTaskInfo(workTaskInfo));
                eventManager.Trigger(new TaskStateChangeEventData
                {
                    WorkTask = workTask,
                    WorkTaskStatus = workTask.WorkTaskStatus,
                });
            }
        }
        /// <summary>
        /// 检查并重置流程状态
        /// </summary>
        /// <param name="workTask"></param>
        /// <param name="workTaskInfo"></param>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        private async Task CheckAndSetTaskProcessed(WorkTask workTask, WorkTaskInfo workTaskInfo, WorkflowNode currentNode)
        {
            ///如果当前节点是开始节点，则流程状态更新为待处理
            if (currentNode.NodeType == WorkNodeType.End)
            {
                workTask.SetProcessed();
                await workTaskRepository.UpdateAsync(workTask.ToWorkTaskInfo(workTaskInfo));

                eventManager.Trigger(new TaskFinishedEventData
                {
                    WorkTask = workTask
                });


                eventManager.Trigger(new TaskStateChangeEventData
                {
                    WorkTask = workTask,
                    WorkTaskStatus = workTask.WorkTaskStatus,
                });
            }
        }

        /// <summary>
        /// 转发
        /// </summary>
        /// <param name="workStepId"></param>
        /// <param name="handleUsers">转发的目标成员</param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<ProveResult> Forward(Guid workStepId, List<NodeUser> UserSelectors, string comment = null)
        {

            await Task.CompletedTask;

            if (UserSelectors == null || UserSelectors.Count == 0)
                return ProveResult.Failed("未提供转办的用户或转办用户无效！"); ;

            var currentWorkStepInfo = await workStepRepository.GetAsync(workStepId);
            var currentWorkStep = currentWorkStepInfo.ToWorkStep();
            var workTaskInfo = await workTaskRepository.GetAsync(currentWorkStep.WorkTaskId);
            var workTask = workTaskInfo.ToWorkTask();
            var steps = new List<WorkStep>();


            //设置当前节点为转发状态
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                currentWorkStep.Handle(WorkStepHandleType.Forward, comment);
                await workStepRepository.UpdateAsync(currentWorkStep.ToWorkStepInfo(currentWorkStepInfo));
                //插入新处理人的处理步骤
                //遍历用户 选择器获取实际用户 并转发处理
                foreach (var selector in UserSelectors)
                {
                    var userSelector = userSelectorManager.GetUserSelector(selector.SelectorId);
                    foreach (var section in selector.Selections)
                    {
                        var _users = userSelector.GetUsers(new SelectorInput
                        {
                            SelectionId = section.Id,
                            Expression = selector.Parameter,
                            WorkTask = workTask
                        });

                        _users.ForEach(user =>
                        {
                            var newStep = currentWorkStep.Copy();
                            newStep.SetHandleUser(user);
                            newStep.FromForward(currentWorkStep.Id);
                            workStepRepository.InsertAsync(newStep.ToWorkStepInfo());
                            steps.Add(newStep);
                        });
                    }

                }
                if (steps.Count == 0)
                    return ProveResult.Failed("找不到可以处理的下一个步骤！");
                return unitOfWork.Commit(ProveResult.Succeed(steps), ProveResult.Failed("提交失败"));
            }
        }

        /// <summary>
        /// 获取正在处理的会签任务
        /// </summary>
        /// <param name="currentNodeId"></param>
        /// <param name="stepGroupId"></param>
        /// <returns></returns>
        private async Task<long> GetOnHandingSignStepsCount(Guid currentNodeId, string stepGroupId)
        {
            await Task.CompletedTask;

            var steps = await workStepRepository.GetCountAsync(ws => ws.GroupId == stepGroupId && ws.IsHandled == false && ws.NodeId != currentNodeId);
            return steps;
        }

        /// <summary>
        /// 获取审批步骤
        /// </summary>
        /// <param name="fromNodeId"></param>
        /// <param name="fromNodeName"></param>
        /// <param name="workTask"></param>
        /// <param name="node"></param>
        /// <param name="preStepGroupId">上一步骤分组id</param>
        /// <param name="nextStepGroupId">下一步骤分组id</param>
        /// <returns></returns>
        private List<WorkStep> GetApproveSteps(Guid fromNodeId, string fromNodeName, WorkTask workTask, WorkflowNode node, string preStepGroupId, string nextStepGroupId)
        {
            List<WorkStep> steps = new List<WorkStep>();
            var nodeUsers = node.GetHandleUsers(workTask, userSelectorManager);

            foreach (var nodeUser in nodeUsers)
            {
                foreach (var user in nodeUser.Value)
                {
                    steps.Add(new WorkStep(Guid.NewGuid(), workTask.Id, fromNodeId, fromNodeName, node.Id, node.Name, user, nodeUser.Key == NodeUser.NodeHandleType.Handle ? WorkStepType.Handle : WorkStepType.ReadOnly, nextStepGroupId, preStepGroupId));
                }
            }
            return steps;
        }
        /// <summary>
        /// 根据选择器获取审批用户
        /// </summary>
        /// <param name="UserSelectors"></param>
        /// <param name="workTask"></param>
        /// <param name="userHandler"></param>
        private void GetUsersByUserSelectors(List<NodeUser> UserSelectors, WorkTask workTask, Action<User> userHandler)
        {
            GetUsersByUserSelectors(UserSelectors, workTask, (selector, user) => userHandler?.Invoke(user));
        }
        /// <summary>
        /// 根据选择器获取审批用户
        /// </summary>
        /// <param name="UserSelectors"></param>
        /// <param name="workTask"></param>
        /// <param name="userHandler"></param>
        private void GetUsersByUserSelectors(List<NodeUser> UserSelectors, WorkTask workTask, Action<NodeUser, User> userHandler)
        {
            foreach (var selector in UserSelectors)
            {
                var userSelector = userSelectorManager.GetUserSelector(selector.SelectorId);
                foreach (var selection in selector.Selections)
                {
                    var _users = userSelector.GetUsers(new SelectorInput
                    {
                        SelectionId = selection.Id,
                        Expression = selector.Parameter,
                        WorkTask = workTask
                    });

                    foreach (var user in _users)
                    {
                        userHandler?.Invoke(selector, user);
                    }

                }
            }
        }



        /// <summary>
        /// 分配任务到处理人员
        /// </summary>
        /// <param name="workTask"></param>
        /// <param name="workSteps"></param>
        /// <param name="formData"></param>
        /// <returns></returns>
        private async Task SendTasks(WorkTask workTask, List<WorkStep> workSteps, string formData = null)
        {

            foreach (var item in workSteps)
            {
                item.IsHandled = false;
                item.SetFormData(formData);
                await workStepRepository.InsertAsync(item.ToWorkStepInfo());
                //TODO 发布开启任务事件
                //TODO 发布开启消息
                //TODO 考虑增加让步骤保存每次处理的表单信息
            }
            eventManager.Trigger(new SendTaskEventData
            {
                WorkTask = workTask,
                WorkSteps = workSteps
            });
        }

        /// <summary>
        /// 获取流程所有过程
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<List<WorkStep>> GetAllTaskStepsOfWorkTaskAsync(Guid workTaskId)
        {
            await Task.CompletedTask;
            return (await workStepRepository.GetListAsync(ws => ws.WorkTaskId == workTaskId)).OrderByDescending(ws => ws.CreationTime).Select(s => s.ToWorkStep()).ToList();
        }

        /// <summary>
        /// 获取流程所有过程
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<List<WorkStep>> GetAllTaskStepsOfWorkTaskByEntityInfoAsync(string entityFullName, string entityKeyValue)
        {
            var worktasks = await workTaskRepository.GetListAsync(t => t.EntityFullName == entityFullName && t.EntityKeyValue == entityKeyValue);
            return (await workStepRepository.GetListAsync(ws => worktasks.Select(w => w.Id).Contains(ws.WorkTaskId))).OrderByDescending(ws => ws.CreationTime).Select(s => s.ToWorkStep()).ToList();
        }

        /// <summary>
        /// 获取流程信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<WorkTask> GetWorkTaskAsync(Guid workTaskId)
        {
            return (await workTaskRepository.GetAsync(workTaskId)).ToWorkTask();
        }


        /// <summary>
        /// 获取用户所有的流程任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResult<WorkTask>> GetAllTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            return await workTaskRepository.GetAllTasksOfUserAsync(userId, pageIndex, pageSize);

        }


        /// <summary>
        /// 获取用户已处理的流程任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResult<WorkTask>> GetWorkflowedTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            return await workTaskRepository.GetWorkflowedTasksOfUserAsync(userId, pageIndex, pageSize);
        }

        /// <summary>
        /// 获取用户未处理的流程任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResult<WorkTask>> GetUnHandledWorkTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            return await workTaskRepository.GetUnHandledWorkTasksOfUserAsync(userId, pageIndex, pageSize);
        }

        /// <summary>
        /// 获取用户处理过的流程任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResult<WorkTask>> GetHandledWorkTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            return await workTaskRepository.GetHandledWorkTasksOfUserAsync(userId, pageIndex, pageSize);
        }


        /// <summary>
        /// 用户发起的流程
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResult<WorkTask>> GetTasksOfStartUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            return await workTaskRepository.GetTasksOfStartUserAsync(userId, pageIndex, pageSize);
        }


        /// <summary>
        /// 获取选择器源数据,可提供前台选择
        /// </summary>
        /// <param name="selectorId"></param>
        /// <returns></returns>
        public async Task<List<Selection>> GetUserSelectionsOfUserSelector(string selectorId)
        {
            await Task.CompletedTask;
            if (string.IsNullOrEmpty(selectorId)) return new List<Selection>();
            var userSelector = userSelectorManager.GetUserSelector(selectorId);
            return userSelector.GetSelections();
        }

        /// <summary>
        /// 删除流程
        /// 同时删除审批记录
        /// </summary>
        /// <param name="worktaskId"></param>
        /// <returns></returns>
        public async Task DeleteWorkTask(Guid worktaskId)
        {
            await workStepRepository.DeleteManyAsync(ws => ws.WorkTaskId == worktaskId);
            await workTaskRepository.DeleteAsync(worktaskId);
        }

        #endregion
    }
}
