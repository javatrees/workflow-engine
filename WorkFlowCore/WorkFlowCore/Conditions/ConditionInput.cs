﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Conditions
{
    /// <summary>
    /// 转换器输入
    /// </summary>
    public class ConditionInput
    {
        /// <summary>
        /// 表达式
        /// </summary>
        public string Expression { get; set; }
        /// <summary>
        /// 工作任务
        /// </summary>
        public WorkTask WorkTask { get; set; }
        /// <summary>
        /// 当前步骤
        /// </summary>
        public WorkStep CurrentWorkStep { get; set; }
        
    }
}
