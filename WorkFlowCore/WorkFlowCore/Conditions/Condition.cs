﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Conditions
{
    public class Condition
    {
        public Condition()
        {
        }

        public Condition(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public Condition(string id, string name, Type type, string description)
        {
            Id = id;
            Name = name;
            ConditionType = type;
            Description = description;
        }
        /// <summary>
        /// 转换器id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 转换器名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 转换器类型
        /// </summary>
        public Type ConditionType { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
