﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    /// <summary>
    /// 用户选择器
    /// </summary>
    public class UserSelector
    {
        public UserSelector(string id, string name, Type selectorType, string description = null)
        {
            Id = id;
            Name = name;
            SelectorType = selectorType;
            Description = description;
        }
        /// <summary>
        /// 选择器id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 选择器名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择器类型
        /// </summary>
        public Type SelectorType { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
