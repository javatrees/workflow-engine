﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    public class User
    {
        public User()
        {
        }

        public User(string handleUser_Id, string handleUser_Name)
        {
            this.Id = handleUser_Id;
            this.Name = handleUser_Name;
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Id + "_" + Name;
        }
    }
}
