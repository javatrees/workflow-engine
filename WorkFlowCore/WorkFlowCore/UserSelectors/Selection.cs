﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    /// <summary>
    /// 选择资源
    /// </summary>
    public class Selection
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
