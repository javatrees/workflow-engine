﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    /// <summary>
    /// 默认用户选择器
    /// 标记了该选择器后，如果节点未指定任何用户选择器，将自动根据默认选择器选中
    /// </summary>
    public class DefaultUserSelectorAttribute: Attribute
    {
        public DefaultUserSelectorAttribute(string defaultSelection = null)
        {
            DefaultSelection = defaultSelection;
        }
        /// <summary>
        /// 默认选项，不指定默认选第一个
        /// </summary>
        public string DefaultSelection { get; set; }
    }
}
