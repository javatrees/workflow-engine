﻿using WorkFlowCore.EventBus;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventData
{
    public class TaskFinishedEventData : BaseEventData
    {
        public WorkTasks.WorkTask WorkTask { get; set; }
    }
}
