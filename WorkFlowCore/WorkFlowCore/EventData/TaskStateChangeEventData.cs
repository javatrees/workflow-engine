﻿using WorkFlowCore.EventBus;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.EventData
{
    public class TaskStateChangeEventData : BaseEventData
    {
        public WorkTasks.WorkTask WorkTask { get; set; }
        public WorkTaskStatus WorkTaskStatus { get; set; }
        public List<WorkTasks.WorkStep> WorkSteps { get; set; }
    }
}
