﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkFlowCore
{
    public class PageResult<T>
    {
        public List<T> Items { get; set; }
        public long Total { get; set; }

        public static PageResult<T> Result(List<T> items,long total)
        {
            var result = new PageResult<T>()
            {
                Items = items,
                Total = total,
            };
            return result;
        }
    }
}
