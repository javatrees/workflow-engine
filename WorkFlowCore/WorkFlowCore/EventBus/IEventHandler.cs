﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventBus
{
    public interface IEventHandler
    {
        
    }

    public interface IEventHandler<TData>: IEventHandler where TData:BaseEventData
    {
        void Handle(TData data);
    }
}
