﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventBus
{
    public interface IEventBus
    {
        void SubscribeEventHandler(Type eventDataType, Type handlerType);
        void UnsubscribeEventHandler(Type eventDataType, Type handlerType);
        void SubscribeEventHandler<TData, THandler>() where THandler : IEventHandler<TData> where TData : BaseEventData;
        void UnsubscribeEventHandler<TData, THandler>() where THandler : IEventHandler<TData> where TData : BaseEventData;
        void Trigger<TData>(TData data);
    }
}
