﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventBus.Implements.Kafka
{
    public class KafkaEventTopicAttribute: Attribute
    {
        public string Topic { get; set; }

        public KafkaEventTopicAttribute(string topic=null)
        {
            Topic = topic;
        }
    }
}
