﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace WorkFlowCore.EventBus.Implements.Kafka
{
    public class KafkaEventConfig
    {
        public string Servers { get; set; }
        public Assembly[] RegisterAssemblies { get; set; }
    }
}
