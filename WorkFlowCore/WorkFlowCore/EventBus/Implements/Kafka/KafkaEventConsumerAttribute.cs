﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventBus.Implements.Kafka
{
    public class KafkaEventConsumerAttribute : Attribute
    {
        public string GroupId { get; set; }

        public KafkaEventConsumerAttribute(string groupId=null)
        {
            GroupId = groupId;
        }
    }
}
