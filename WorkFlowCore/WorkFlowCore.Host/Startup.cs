using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WorkFlowCore.EventBus;
using WorkFlowCore.Framework;
using WorkFlowCore.Framework.Repositories4EF;

namespace WorkFlowCore.Host
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            services.AddControllersWithViews();
            //注册流程引擎
            services.AddWorkFlowCore();
            services.AddWorkFlowCoreFramework(options=> {
                options.OrmType = WorkFlowCoreFrameworkService.FrameworkConfigOrmType.EF;
            });
            services.AddDbContext<WorkflowDbContext>(op =>
            {
                op.UseMySql(Configuration.GetConnectionString("Default"), new MySqlServerVersion(Configuration.GetConnectionString("DefaultVersion")));
            });

            //注册swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new OpenApiInfo { Title = "流程服务", Version = "1.0" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // Configure CORS for angular2 UI
            services.AddCors(
                options => options.AddPolicy(
                    "defaultcors",
                    builder => builder
                        .WithOrigins(
                            // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                            //Configuration["AllowedHosts"]
                            //    .Split(",", StringSplitOptions.RemoveEmptyEntries)
                            //    .ToArray()
                            "http://localhost:9528".Split(',')
                        )
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                )
            );

            services.AddDefautEventBus(typeof(WorkFlowCoreFrameworkService).Assembly);
            var KafkaBootstrapServers = Configuration["KafkaBootstrapServers"] ?? Configuration.GetSection("Kafka")["BootstrapServers"];
            if (!string.IsNullOrEmpty(KafkaBootstrapServers))
            {
                services.AddKafkaEventBus(config =>
                {
                    config.Servers = KafkaBootstrapServers;
                    config.RegisterAssemblies = new Assembly[] { typeof(WorkFlowCoreFrameworkService).Assembly };
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors("defaultcors");
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "流程服务(V 1.0)");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "api",
                    pattern: "api/{controller=Home}/{action=Index}/{id?}");
            });

            //注册全局事件总线
            app.InitGlobalEventBus();
        }
    }
}
