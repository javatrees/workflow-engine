﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class ForwardProveInput:ProveInput
    {
        public List<NodeUser> UserSelectors { get; set; }
    }
}
