﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class WorkflowLineDto
    {
        public string Name { get; set; }
        public Guid FromNodeId { get; set; }
        public Guid ToNodeId { get; set; }

        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        public List<LineCondition> Conditions { get; set; }
    }
}
